## **PragmaTeam Technical Challenge**

Code implementation challenge due to candidates to become an coop of PragmaTeam.
The challenge rules can be found [here](https://drive.google.com/file/d/0Bzav_TVrRBoJaHl5LUh4ZmtQX3FWZDZJckpkT1VOUG9oOVdJ/view?usp=sharing).

The code can be found into these links: [Backend](https://bitbucket.org/rumayorjohnathan/pragmateam-backend-test/) | [Frontend](https://bitbucket.org/rumayorjohnathan/pragmateam-ui-test/)

Both have an Dockerfile and can be executed apart.

Docker images already built and pushed do Docker Hub:
[johnathanrumayor/pragmateam-backend](https://cloud.docker.com/u/johnathanrumayor/repository/docker/johnathanrumayor/pragmateam-backend)
[johnathanrumayor/pragmateam-ui](https://cloud.docker.com/u/johnathanrumayor/repository/docker/johnathanrumayor/pragmateam-ui)

Technologies used:
 - Backend developed in Java, builted as an ear composed by an ejb module and a war, this solution is able to be escalated in the future if necessary and also it can has its modules segregated to improve the performance and availability.
 - Deployed into Wildfly Application Server to provide the main system controls easily configured, and possible to be executed as Domain mode with multiple instances.
 - The user interface was built as an Angular App, on node support, calling a backend rest api and showing the formatted results to regular users.
 
Improvents: Integrate a swagger to the project, to simply show the apis and its contracts.

**H2R: How To Run**
- The running operational system must have support to Hyper Virtualization, Docker and Docker-Compose services installed and running.
- Clone this repository or simply download the docker-compose.yml file.
- Into the folder that contains the files, run the command:
> docker-compose -f docker-compose.yml up --build

- The logs will be available at command line and the user interface will be shown at port 4200 of the localhost. http://localhost:4200/

**H2T: How To Test**
- The tests are able to be runned since your environment runs an [Application Server Wildfly 9.0](https://download.jboss.org/wildfly/9.0.2.Final/wildfly-9.0.2.Final.zip).
- The environment must be configured with Java 8+ and Maven 3+
- Into the cloned project, open a terminal and execute the command: 
> mvn clean test -Parq-wildfly-remote

Is possible to change manually the beers data using the resources created at backend api using RESTful endpoints.

GET Path("") to list all (OK-200)
GET Path("/{id:[0-9][0-9]*}") to find a beer (OK-200 | NOT_FOUND-404)
POST Path("") to create a beer (CREATED-201 | CONFLICT-409 | NOT_FOUND-404)
PUT Path("/{id:[0-9][0-9]*}") to update a beer (OK-200 | NOT_FOUND-404)
DELETE Path("/{id:[0-9][0-9]*}") to delete a beer (NO_CONTENT-204 | NOT_FOUND-404)

| ------ | ------ |

> Any doubts of questions can be delivered to rumayorjohnathan@gmail.com. I'd be glad to switch ideias about this solution.
